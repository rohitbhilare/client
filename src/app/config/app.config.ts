export class AppConfig {
  public readonly apiUrl = 'http://localhost:4500/api/'; 
  public pageSize = 5;
  public pageSizeOptions: number[] = [5, 10, 25, 100];
}
