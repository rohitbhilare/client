import { Component, OnInit } from '@angular/core';
import { FeedService } from './feed.service';
import { AppConfig } from '../config/app.config';
import { filterModel } from './feed.model';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css']
})
export class FeedComponent implements OnInit {
  pageSizeOptions: number[];
  feedData: any;
  totalCount: number = 0;
  filterModel: filterModel = new filterModel();


  constructor(private feedService: FeedService) {
    this.pageSizeOptions = new AppConfig().pageSizeOptions;
    this.filterModel.pageSize = new AppConfig().pageSize;
  }


  searchByKeyword() {
    this.filterModel.pageNumber = 1;
    this.getPaginatedData();
  }

  sortData(evt) {
    this.filterModel.isSortedbyTitle = false;
    this.filterModel.isSortedbyUpdatedDate = false;

    if (evt.value.includes('Title'))
      this.filterModel.isSortedbyTitle = true
    else
      this.filterModel.isSortedbyTitle = false;
    if (evt.value.includes("LastEdited"))
      this.filterModel.isSortedbyUpdatedDate = true;
    else
      this.filterModel.isSortedbyUpdatedDate = false;

    this.getPaginatedData();

  }

  resetFilter(){
    this.filterModel.keyword = "";
    this.filterModel.isSortedbyTitle = this.filterModel.isSortedbyUpdatedDate = false;
    this.getPaginatedData();

  }


  async getPaginatedData() {
    this.feedData = await this.feedService.getFeedByPaginate(this.filterModel).toPromise();
    this.totalCount = this.feedData.totalcount;
  }

  paginatedData(evt) {
    this.filterModel.pageSize = evt.pageSize;
    this.filterModel.pageNumber = evt.pageIndex + 1;
    this.getPaginatedData();

  }

  ngOnInit() {
    this.filterModel.pageNumber = 1;
    this.getPaginatedData();
  }

}
