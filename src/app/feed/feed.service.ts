import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppConfig } from '../config/app.config';

@Injectable({ providedIn: 'root' })

export class FeedService {
  apiUrl: string;
  constructor(private httpClient: HttpClient) {
    this.apiUrl = new AppConfig().apiUrl;
  }
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  getFeedByPaginate(filterModel): Observable<any> {
    return this.httpClient.post<any>(this.apiUrl + "feed", filterModel, this.httpOptions).pipe();

  }
}
