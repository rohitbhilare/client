export class filterModel {
  pageNumber: number;
  pageSize: number;
  keyword: string;
  isSortedbyTitle: boolean;
  isSortedbyUpdatedDate: boolean;
}
